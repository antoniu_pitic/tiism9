#include "Recapitulare.h"
#include <iostream>
using namespace std;

bool CrescPeDiagPrinc(int A[10][10], int n)
{

	for (int i = 0; i < n - 1; i++)
	{
		if (A[i][i] > A[i + 1][i + 1]) {
			return false;
		}
	}
	return true;
}

bool DescrescPeDiagPrinc(int A[10][10], int n)
{
	for (int i = 0; i < n-1; i++)
	{
		if (A[i][i] < A[i + 1][i + 1]) {
			return false;
		}
	}
	return true;
}

bool DescrescPeDiagSec(int A[10][10], int n)
{

	for (int i = 0; i < n - 1; i++)
	{
		if (A[i][n - 1 - i] < A[i + 1][n - i - 2]) {
			return false;
		}
	}
	return true;

}

void Citeste(int A[10][10], int & n)
{
	cin >> n;

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			cin >> A[i][j];
		}
	}
}

void Afisare(int A[10][10], int & n)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			cout.width(3);
			cout << A[i][j];
		}
		cout << endl;
	}
}

bool CrescPeDiagSec(int A[10][10], int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		if (A[i][n-1-i] > A[i+1][n-2-i]) {
			return false;
		}
	}
	return true;
}