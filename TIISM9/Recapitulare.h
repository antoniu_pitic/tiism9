#pragma once

bool CrescPeDiagPrinc(int A[10][10], int n);
bool DescrescPeDiagPrinc(int A[10][10], int n);
bool DescrescPeDiagSec(int A[10][10], int n);
bool CrescPeDiagSec(int A[10][10], int n);

void Citeste(int A[10][10], int &n);
void Afisare(int A[10][10], int &n);
