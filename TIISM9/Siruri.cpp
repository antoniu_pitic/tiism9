#include "Siruri.h"

// toni -> toponipi
// inginerie -> ipingipineperipiepe
// liniste -> lipinipistepe

void Pasareste(char S[100], char P[300])
{
	int j = 0;
	for (int i = 0; S[i] ; i++)
	{
		P[j++] = S[i];
		if (S[i]=='a' ||S[i]=='e' ||S[i]=='i' ||
			S[i]=='o' ||S[i]=='u') {
			P[j++] = 'p';
			P[j++] = S[i];
		}
	}
	
	P[j] = 0;

}

void Depasareste(char S[300], char D[100])
{
	int j = 0;
	for (int i = 0; S[i]; i++)
	{
		D[j++] = S[i];
		if (S[i] == 'a' || S[i] == 'e' || S[i] == 'i' ||
			S[i] == 'o' || S[i] == 'u') {
			i += 2;
		}
	}

	D[j] = 0;
}

void Criptare(char S[100])
{
	for (int i = 0; S[i] ; i++)
	{
		S[i] ^= 'X';
		S[i] += i;
	}
}

void Decriptare(char S[100])
{	
	for (int i = 0; S[i] ; i++)
	{
		S[i] -= i;
		S[i] ^= 'X';
		
	}

}
